#include <sourcemod>
#include <sdktools>

// Define the zombie models and their corresponding skills
new String:g_ZombieModels[][] = {
    {"boomer", "models/zombie/boomer.mdl", "Explode after death, makes some damage also blinds nearby humans"},
    {"classical", "models/zombie/classical.mdl", "No special abilities"},
    {"fury", "models/zombie/fury.mdl", "Spawns after some time, this zombie is key to victory"},
    {"hunter", "models/zombie/hunter.mdl", "Make really crazy jumps"},
    {"phantom", "models/zombie/phantom.mdl", "Possibility to make himself full invisible"},
    {"smoker", "models/zombie/smoker.mdl", "Can drag person with his tongue, makes smoke effect after death"}
};

// Define the zombie skills
new g_BoomerExplode;
new g_FurySpawnTime;
new g_HunterJumpHeight;
new g_PhantomInvisibilityDuration;
new g_SmokerTongueRange;

public Plugin myinfo = 
{
    name = "Zombie Skills",
    author = "Your Name",
    description = "Zombie skills plugin",
    version = "1.0",
    url = "https://www.example.com"
};

public OnPluginStart()
{
    // Automatically enable the plugin when zombieriot.smx is run
    g_ZombieSkillsEnabled = true;
    
    // Register the zskill command
    RegConsoleCmd("zskill", Command_ZSkill);
    
    // Initialize the zombie skills
    g_BoomerExplode = CreateEntityByName("env_explosion");
    g_FurySpawnTime = 300; // 5 minutes
    g_HunterJumpHeight = 500.0;
    g_PhantomInvisibilityDuration = 10.0;
    g_SmokerTongueRange = 200.0;
}

public Action Command_ZSkill(client, args)
{
    if (IsClientInGame(client) && IsPlayerAlive(client) && GetClientTeam(client) == 3) // 3 is the zombie team
    {
        new String:model[64];
        GetClientModel(client, model, sizeof(model));
        
        for (new i = 0; i < sizeof(g_ZombieModels); i++)
        {
            if (StrEqual(model, g_ZombieModels[i][1]))
            {
                PrintToChat(client, "\x04[Zombie Skills] You are a %s zombie!", g_ZombieModels[i][0]);
                PrintToChat(client, "\x04[Zombie Skills] Your skill: %s", g_ZombieModels[i][2]);
                
                // Display the skill menu
                new Handle:menu = CreateMenu(Menu_ZSkill);
                SetMenuTitle(menu, "Zombie Skills");
                AddMenuItem(menu, "Use Skill", "Use your zombie skill");
                DisplayMenu(menu, client, MENU_TIME_FOREVER);
                
                break;
            }
        }
    }
    return Plugin_Handled;
}

public int Menu_ZSkill(Handle:menu, MenuAction:action, param1, param2)
{
    if (action == MenuAction_Select)
    {
        new client = param1;
        new String:model[64];
        GetClientModel(client, model, sizeof(model));
        
        for (new i = 0; i < sizeof(g_ZombieModels); i++)
        {
            if (StrEqual(model, g_ZombieModels[i][1]))
            {
                // Use the corresponding zombie skill
                switch (i)
                {
                    case 0: // Boomer
                        CreateTimer(0.1, Timer_BoomerExplode, client);
                    case 1: // Classical
                        // No special abilities
                        break;
                    case 2: // Fury
                        CreateTimer(g_FurySpawnTime, Timer_FurySpawn, client);
                    case 3: // Hunter
                        SetEntityGravity(client, 0.5);
                        SetEntPropFloat(client, Prop_Data, "m_flJumpHeight", g_HunterJumpHeight);
                    case 4: // Phantom
                        CreateTimer(g_PhantomInvisibilityDuration, Timer_PhantomInvisibility, client);
                    case 5: // Smoker
                        CreateTimer(0.1, Timer_SmokerTongue, client);
                }
                break;
            }
        }
    }
    return 0;
}

public Action Timer_BoomerExplode(Handle:timer, any client)
{
    // Explode the boomer zombie
    g_BoomerExplode.SetOrigin(GetClientAbsOrigin(client));
    AcceptEntityInput(g_BoomerExplode, "Explode");
    RemoveEntity(g_BoomerExplode);
    return Plugin_Stop;
}

public Action Timer_FurySpawn(Handle:timer, any client)
{
    // Spawn the fury zombie
    PrintToChatAll("\x04[Zombie Skills] Fury zombie spawned!");
    return Plugin_Stop;
}

public Action Timer_PhantomInvisibility(Handle:timer, any client)
{
    // Make the phantom zombie invisible
    SetEntityRenderMode(client, RENDER_NORMAL);
    return Plugin_Stop;
}

public Action Timer_SmokerTongue(Handle:timer, any client)
{
    // Drag a person with the smoker zombie's tongue
    new target = GetClosestPlayer(client, g_SmokerTongueRange);
    if (target != -1)
    {
        SetEntPropEnt(client, Prop_Data, "m_hTongue", target);
    }
    return Plugin_Stop;
}